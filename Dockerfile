FROM debian:stretch
RUN apt-get update && yes | apt-get upgrade
RUN apt-get install -y git libsm6 libxrender-dev protobuf-compiler
Run apt-get install -y python3-pip libopenjp2-7 libtiff5 python3-lxml
RUN mkdir -p /tensorflow/models
COPY build/ts-models/models /tensorflow/models
COPY build/Custom-TensorFlow-Object-Detection-API/ /tensorflow/models/research/object_detection
COPY build/training /tensorflow/models/research/object_detection/training
COPY build/inference_graph /tensorflow/models/research/object_detection/inference_graph
# COPY  requiremnts/ /tensorflow/models/research/object_detection
WORKDIR /tensorflow/models/research/object_detection
WORKDIR /tensorflow/models/research

# Tensorflow_object_detection_api_dependecies
RUN pip3 install --upgrade tensorflow
RUN pip3 install jupyter
RUN pip3 install matplotlib
RUN pip3 install Cython
RUN pip3 install pandas
RUN pip3 install opencv-python
# RabbitMQ dependecies
RUN pip3 install pika

# for gitlab ci test
RUN pip3 install docker-compose
#RUN apk add --no-cache bash
#Run pip3 install attr
#Run pip3 install attrs
RUN pip3 install pytest
RUN pip3 install --upgrade autopep8
RUN python3 -m pip install -U mypy
RUN pip3 install pylint
RUN pip3 install flake8
#unittest dependecies
RUN pip3 install unittest2
RUN pip3 install parameterized

# sphinx template
# pip install cakephp_theme
# pip install yummy-sphinx-theme
RUN protoc object_detection/protos/*.proto --python_out=.
RUN export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
RUN python3 setup.py build
RUN python3 setup.py install

RUN jupyter notebook --generate-config --allow-root
RUN echo "c.NotebookApp.ip = '0.0.0.0'" >> ~/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.allow_remote_access = True" >> ~/.jupyter/jupyter_notebook_config.py
RUN echo "c.MultiKernelManager.default_kernel_name = 'python3'" >> ~/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.port = 8080" >> ~/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.password = u'sha1:6a3f528eec40:6e896b6e4828f525a6e20e5411cd1c8075d68619'" >> /root/.jupyter/jupyter_notebook_config.py
EXPOSE 8888
CMD ["jupyter", "notebook", "--allow-root", "--notebook-dir=/tensorflow/models/research/object_detection", "--ip='*'", "--port=8888", "--no-browser", "--allow-root"]


