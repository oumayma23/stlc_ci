# STL

This project is the first phase of the realization of a global automatic system for the monitoring and control of road traffic.
In this Readme i'll show how i setup a GitLab CI process to run the following jobs on our project:
```
	-Linting using flake8
	-Static analysis using pylint
	-Type checking using mypy
	-Automatically formats Python code using autopep8
```
## What is CI?
To me, Continuous Integration (CI) means frequently testing your application in an integrated state.
In our case we will use only:
```
	-Static analysis
	-Style checking (linting)
	-Dynamic analysis
	-Automatically formats
```
To facilitate running these tests, it’s best to have these tests run automatically as part of your configuration management (git) process.
I’ve found it really beneficial to develop a test script locally and then add it to the CI process that gets automatically run on GitLab CI/CD.

### Getting Started with GitLab CI/CD

Before jumping into GitLab CI/CD , here are a few definitions:

```
– pipeline: a set of tests to run against a single git commit.

– runner: GitLab uses runners on different servers to actually execute the tests in a pipeline; GitLab provides runners to use, but you can also spin up your own servers as runners.

– job: a single test being run in a pipeline.

– stage: a group of related tests being run in a pipeline.
```

### Installing
You can install up your own servers as runners, in our project we will install a specific runner in the local machine
 
```
1.Install GitLab Runner using this link: 
https://docs.gitlab.com/runner/install/
```
Then
```
2.Registering Runner:
Registering a Runner is the process that binds the Runner with a GitLab instance.
follow this link:
https://docs.gitlab.com/runner/register/#windows
-Use this tags:
    - docker-in-docker-builder
-Use a shell executer
```

## Creating The First Job in GitLab CI: Flake8

The first job that I want to add to GitLab CI/CD for my project is to run a linter (flake8).  
In my local development environment, I would run this command:
```
docker exec -i ${CONTAINER_NAME} flake8 object_detection/test_generate_tfrecord.py
```
This command can be transformed into a job on GitLab CI in the ‘.gitlab-ci.yml’ file:

```
flake8:
  stage: Static Analysis
  before_script:
    - docker container stop ${CONTAINER_NAME}
    - docker container rm ${CONTAINER_NAME}
  tags:
    - docker-in-docker-builder
  script: 
  - docker run -d --name ${CONTAINER_NAME} registry.gitlab.com/ben-sassi-oussema/stlc_ci_cd
  - docker exec -i ${CONTAINER_NAME} flake8 object_detection/test_generate_tfrecord.py 
```
### Explaination
```
-The section (before_script) is the set of commands to run in the Docker container before starting each job. 
This is really beneficial for getting the Docker container in the correct state by installing all the python packages needed in the project.
-The section (stage) defines the different stages in the pipeline. There is only a single stage (Static Analysis) at this point. I like to think of stages as a way to group together related jobs.
-The section (flake8) defines the job; it specifies the stage (Static Analysis) that the job should be part of and the commands to run in the Docker container for this job.
-Flake8: Your Tool For Style Guide Enforcement, you can visit this link for more information:
http://flake8.pycqa.org/en/latest/
```

<p align="center">
  <img src="images/flake8.PNG">
</p>

GitLab CI will run a linter (flake8) on every commit that is pushed up to GitLab for this project.

## Creating The Second Job in GitLab CI: Mypy
```
-Mypy is an optional static type checker for Python that aims to combine the benefits of dynamic typing and static typing. 
-Mypy combines the expressive power and convenience of Python with a powerful type system and compile-time type checking. 
-Mypy type checks standard Python programs.
The first job that I want to add to GitLab CI/CD for my project is to run a linter (flake8).  
```

In my local development environment, I would run this command:

```
docker exec -i ${CONTAINER_NAME} mypy object_detection/test_generate_tfrecord.py  --ignore-missing-imports
```
This command can be transformed into a job on GitLab CI in the ‘.gitlab-ci.yml’ file:

```
mypy:
  stage: Static Analysis
  before_script:
    - docker container stop ${CONTAINER_NAME}
    - docker container rm ${CONTAINER_NAME}
 
  tags:
    - docker-in-docker-builder
  script:
  - docker run -d --name ${CONTAINER_NAME} registry.gitlab.com/ben-sassi-oussema/stlc_ci_cd
  - docker exec -i ${CONTAINER_NAME} mypy object_detection/test_generate_tfrecord.py  --ignore-missing-imports
```
Here is the resulting output from GitLab CI:


<p align="center">
  <img src="images/mypy.PNG">
</p>

## Creating The Third Job in GitLab CI: PyLint
```
-Pylint is a powerful tool that performs static code analysis and checks that your code is written following Python coding standards (called PEP8).
-You can check this link for more information about (PEP8): https://www.python.org/dev/peps/pep-0008/
-I set the pylint job to be allowed to fail via the ‘allow_failure’ setting
```

In my local development environment, I would run this command:
```
docker exec -i ${CONTAINER_NAME} pylint object_detection/test_generate_tfrecord.py    

```
This command can be transformed into a job on GitLab CI in the ‘.gitlab-ci.yml’ file:

```
pylint:
  stage: Static Analysis
  before_script:
    - docker container stop ${CONTAINER_NAME}
    - docker container rm ${CONTAINER_NAME}  
  allow_failure: true
  
  tags:
    - docker-in-docker-builder
  script:
  - docker run -d --name ${CONTAINER_NAME} registry.gitlab.com/ben-sassi-oussema/stlc_ci_cd
  - docker exec -i ${CONTAINER_NAME} pylint object_detection/test_generate_tfrecord.py       
```
Here is the resulting output from GitLab CI:


<p align="center">
  <img src="images/mypy.PNG">
</p>


## Creating The Fourth Job in GitLab CI: Autopep8
```
-autopep8 automatically formats Python code to conform to the PEP8 style guide. 
-It uses the pycodestyle utility to determine what parts of the code needs to be formatted. 
-autopep8 is capable of fixing most of the formatting issues that can be reported by pycodestyle.
-You can check this link for more information about (PEP8): https://www.python.org/dev/peps/pep-0008/
```
In my local development environment, I would run this command:

```
- docker exec -i ${CONTAINER_NAME} autopep8 --in-place --aggressive --aggressive object_detection/test_generate_tfrecord.py

```
This command can be transformed into a job on GitLab CI in the ‘.gitlab-ci.yml’ file:

```
autopep8:
  stage: Static Analysis
  before_script:
    - docker container stop ${CONTAINER_NAME}
    - docker container rm ${CONTAINER_NAME}
  tags:
    - docker-in-docker-builder
  script:
  - docker run -d --name ${CONTAINER_NAME} registry.gitlab.com/ben-sassi-oussema/stlc_ci_cd
  - docker exec -i ${CONTAINER_NAME} autopep8 --in-place --aggressive --aggressive object_detection/test_generate_tfrecord.py
```

## References

- https://cylab.be/blog/18/gitlab-automatically-testing-your-python-project
- https://realpython.com/python-code-quality/
- https://luminousmen.com/post/python-static-analysis-tools
- https://github.com/python/mypy
- https://github.com/hhatto/autopep8
