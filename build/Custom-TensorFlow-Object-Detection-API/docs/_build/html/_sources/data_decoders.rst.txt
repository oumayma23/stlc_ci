data\_decoders package
======================

Submodules
----------

data\_decoders.tf\_example\_decoder module
------------------------------------------

.. automodule:: data_decoders.tf_example_decoder
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: data_decoders
   :members:
   :undoc-members:
   :show-inheritance:
