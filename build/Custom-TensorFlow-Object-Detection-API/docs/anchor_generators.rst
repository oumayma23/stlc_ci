anchor\_generators package
==========================

Submodules
----------

anchor\_generators.flexible\_grid\_anchor\_generator module
-----------------------------------------------------------

.. automodule:: anchor_generators.flexible_grid_anchor_generator
   :members:
   :undoc-members:
   :show-inheritance:

anchor\_generators.grid\_anchor\_generator module
-------------------------------------------------

.. automodule:: anchor_generators.grid_anchor_generator
   :members:
   :undoc-members:
   :show-inheritance:

anchor\_generators.multiple\_grid\_anchor\_generator module
-----------------------------------------------------------

.. automodule:: anchor_generators.multiple_grid_anchor_generator
   :members:
   :undoc-members:
   :show-inheritance:

anchor\_generators.multiscale\_grid\_anchor\_generator module
-------------------------------------------------------------

.. automodule:: anchor_generators.multiscale_grid_anchor_generator
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: anchor_generators
   :members:
   :undoc-members:
   :show-inheritance:
