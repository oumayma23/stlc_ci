box\_coders package
===================

Submodules
----------

box\_coders.faster\_rcnn\_box\_coder module
-------------------------------------------

.. automodule:: box_coders.faster_rcnn_box_coder
   :members:
   :undoc-members:
   :show-inheritance:

box\_coders.keypoint\_box\_coder module
---------------------------------------

.. automodule:: box_coders.keypoint_box_coder
   :members:
   :undoc-members:
   :show-inheritance:

box\_coders.mean\_stddev\_box\_coder module
-------------------------------------------

.. automodule:: box_coders.mean_stddev_box_coder
   :members:
   :undoc-members:
   :show-inheritance:

box\_coders.square\_box\_coder module
-------------------------------------

.. automodule:: box_coders.square_box_coder
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: box_coders
   :members:
   :undoc-members:
   :show-inheritance:
