core package
============

Submodules
----------

core.anchor\_generator module
-----------------------------

.. automodule:: core.anchor_generator
   :members:
   :undoc-members:
   :show-inheritance:

core.balanced\_positive\_negative\_sampler module
-------------------------------------------------

.. automodule:: core.balanced_positive_negative_sampler
   :members:
   :undoc-members:
   :show-inheritance:

core.batcher module
-------------------

.. automodule:: core.batcher
   :members:
   :undoc-members:
   :show-inheritance:

core.box\_coder module
----------------------

.. automodule:: core.box_coder
   :members:
   :undoc-members:
   :show-inheritance:

core.box\_list module
---------------------

.. automodule:: core.box_list
   :members:
   :undoc-members:
   :show-inheritance:

core.box\_list\_ops module
--------------------------

.. automodule:: core.box_list_ops
   :members:
   :undoc-members:
   :show-inheritance:

core.box\_list\_test module
---------------------------

.. automodule:: core.box_list_test
   :members:
   :undoc-members:
   :show-inheritance:

core.box\_predictor module
--------------------------

.. automodule:: core.box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

core.data\_decoder module
-------------------------

.. automodule:: core.data_decoder
   :members:
   :undoc-members:
   :show-inheritance:

core.data\_parser module
------------------------

.. automodule:: core.data_parser
   :members:
   :undoc-members:
   :show-inheritance:

core.freezable\_batch\_norm module
----------------------------------

.. automodule:: core.freezable_batch_norm
   :members:
   :undoc-members:
   :show-inheritance:

core.keypoint\_ops module
-------------------------

.. automodule:: core.keypoint_ops
   :members:
   :undoc-members:
   :show-inheritance:

core.losses module
------------------

.. automodule:: core.losses
   :members:
   :undoc-members:
   :show-inheritance:

core.matcher module
-------------------

.. automodule:: core.matcher
   :members:
   :undoc-members:
   :show-inheritance:

core.minibatch\_sampler module
------------------------------

.. automodule:: core.minibatch_sampler
   :members:
   :undoc-members:
   :show-inheritance:

core.model module
-----------------

.. automodule:: core.model
   :members:
   :undoc-members:
   :show-inheritance:

core.post\_processing module
----------------------------

.. automodule:: core.post_processing
   :members:
   :undoc-members:
   :show-inheritance:

core.prefetcher module
----------------------

.. automodule:: core.prefetcher
   :members:
   :undoc-members:
   :show-inheritance:

core.preprocessor module
------------------------

.. automodule:: core.preprocessor
   :members:
   :undoc-members:
   :show-inheritance:

core.preprocessor\_cache module
-------------------------------

.. automodule:: core.preprocessor_cache
   :members:
   :undoc-members:
   :show-inheritance:

core.region\_similarity\_calculator module
------------------------------------------

.. automodule:: core.region_similarity_calculator
   :members:
   :undoc-members:
   :show-inheritance:

core.standard\_fields module
----------------------------

.. automodule:: core.standard_fields
   :members:
   :undoc-members:
   :show-inheritance:

core.target\_assigner module
----------------------------

.. automodule:: core.target_assigner
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: core
   :members:
   :undoc-members:
   :show-inheritance:
