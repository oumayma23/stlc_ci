legacy package
==============

Submodules
----------

legacy.eval module
------------------

.. automodule:: legacy.eval
   :members:
   :undoc-members:
   :show-inheritance:

legacy.evaluator module
-----------------------

.. automodule:: legacy.evaluator
   :members:
   :undoc-members:
   :show-inheritance:

legacy.trainer module
---------------------

.. automodule:: legacy.trainer
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: legacy
   :members:
   :undoc-members:
   :show-inheritance:
