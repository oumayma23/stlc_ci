object_detection
================

.. toctree::
   :maxdepth: 4

   anchor_generators
   box_coders
   core
   data_decoders
   export_inference_graph
   exporter
   legacy
   predictors
   protos
   train
