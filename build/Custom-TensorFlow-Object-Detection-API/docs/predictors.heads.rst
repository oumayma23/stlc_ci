predictors.heads package
========================

Submodules
----------

predictors.heads.box\_head module
---------------------------------

.. automodule:: predictors.heads.box_head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.class\_head module
-----------------------------------

.. automodule:: predictors.heads.class_head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.head module
----------------------------

.. automodule:: predictors.heads.head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.keras\_box\_head module
----------------------------------------

.. automodule:: predictors.heads.keras_box_head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.keras\_class\_head module
------------------------------------------

.. automodule:: predictors.heads.keras_class_head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.keras\_mask\_head module
-----------------------------------------

.. automodule:: predictors.heads.keras_mask_head
   :members:
   :undoc-members:
   :show-inheritance:

predictors.heads.mask\_head module
----------------------------------

.. automodule:: predictors.heads.mask_head
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: predictors.heads
   :members:
   :undoc-members:
   :show-inheritance:
