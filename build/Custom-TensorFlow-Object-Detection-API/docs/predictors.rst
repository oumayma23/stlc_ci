predictors package
==================

Subpackages
-----------

.. toctree::

   predictors.heads

Submodules
----------

predictors.convolutional\_box\_predictor module
-----------------------------------------------

.. automodule:: predictors.convolutional_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

predictors.convolutional\_keras\_box\_predictor module
------------------------------------------------------

.. automodule:: predictors.convolutional_keras_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

predictors.mask\_rcnn\_box\_predictor module
--------------------------------------------

.. automodule:: predictors.mask_rcnn_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

predictors.mask\_rcnn\_keras\_box\_predictor module
---------------------------------------------------

.. automodule:: predictors.mask_rcnn_keras_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

predictors.rfcn\_box\_predictor module
--------------------------------------

.. automodule:: predictors.rfcn_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:

predictors.rfcn\_keras\_box\_predictor module
---------------------------------------------

.. automodule:: predictors.rfcn_keras_box_predictor
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: predictors
   :members:
   :undoc-members:
   :show-inheritance:
