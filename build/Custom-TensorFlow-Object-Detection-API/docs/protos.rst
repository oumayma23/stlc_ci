protos package
==============

Submodules
----------

protos.anchor\_generator\_pb2 module
------------------------------------

.. automodule:: protos.anchor_generator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.argmax\_matcher\_pb2 module
----------------------------------

.. automodule:: protos.argmax_matcher_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.bipartite\_matcher\_pb2 module
-------------------------------------

.. automodule:: protos.bipartite_matcher_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.box\_coder\_pb2 module
-----------------------------

.. automodule:: protos.box_coder_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.box\_predictor\_pb2 module
---------------------------------

.. automodule:: protos.box_predictor_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.calibration\_pb2 module
------------------------------

.. automodule:: protos.calibration_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.eval\_pb2 module
-----------------------

.. automodule:: protos.eval_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.faster\_rcnn\_box\_coder\_pb2 module
-------------------------------------------

.. automodule:: protos.faster_rcnn_box_coder_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.faster\_rcnn\_pb2 module
-------------------------------

.. automodule:: protos.faster_rcnn_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.flexible\_grid\_anchor\_generator\_pb2 module
----------------------------------------------------

.. automodule:: protos.flexible_grid_anchor_generator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.graph\_rewriter\_pb2 module
----------------------------------

.. automodule:: protos.graph_rewriter_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.grid\_anchor\_generator\_pb2 module
------------------------------------------

.. automodule:: protos.grid_anchor_generator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.hyperparams\_pb2 module
------------------------------

.. automodule:: protos.hyperparams_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.image\_resizer\_pb2 module
---------------------------------

.. automodule:: protos.image_resizer_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.input\_reader\_pb2 module
--------------------------------

.. automodule:: protos.input_reader_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.keypoint\_box\_coder\_pb2 module
---------------------------------------

.. automodule:: protos.keypoint_box_coder_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.losses\_pb2 module
-------------------------

.. automodule:: protos.losses_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.matcher\_pb2 module
--------------------------

.. automodule:: protos.matcher_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.mean\_stddev\_box\_coder\_pb2 module
-------------------------------------------

.. automodule:: protos.mean_stddev_box_coder_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.model\_pb2 module
------------------------

.. automodule:: protos.model_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.multiscale\_anchor\_generator\_pb2 module
------------------------------------------------

.. automodule:: protos.multiscale_anchor_generator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.optimizer\_pb2 module
----------------------------

.. automodule:: protos.optimizer_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.pipeline\_pb2 module
---------------------------

.. automodule:: protos.pipeline_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.post\_processing\_pb2 module
-----------------------------------

.. automodule:: protos.post_processing_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.preprocessor\_pb2 module
-------------------------------

.. automodule:: protos.preprocessor_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.region\_similarity\_calculator\_pb2 module
-------------------------------------------------

.. automodule:: protos.region_similarity_calculator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.square\_box\_coder\_pb2 module
-------------------------------------

.. automodule:: protos.square_box_coder_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.ssd\_anchor\_generator\_pb2 module
-----------------------------------------

.. automodule:: protos.ssd_anchor_generator_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.ssd\_pb2 module
----------------------

.. automodule:: protos.ssd_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.string\_int\_label\_map\_pb2 module
------------------------------------------

.. automodule:: protos.string_int_label_map_pb2
   :members:
   :undoc-members:
   :show-inheritance:

protos.train\_pb2 module
------------------------

.. automodule:: protos.train_pb2
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: protos
   :members:
   :undoc-members:
   :show-inheritance:
