"""
Generate_tfrecord file

"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
import os
import io
from collections import namedtuple
import pandas as pd
import tensorflow as tf

from PIL import Image
from utils import dataset_util

FLAG = tf.app.flags
FLAG.DEFINE_string('csv_input', '', 'Path to the CSV input')
FLAG.DEFINE_string('image_dir', '', 'Path to the image directory')
FLAG.DEFINE_string('output_path', '', 'Path to output TFRecord')
FLAGS = FLAG.FLAGS


def class_text_to_int(row_label):
    """
  Convert string to integer
  :param row_label:
  :return: integer class
  """
    code = None
    if row_label == 'articulated_truck':
        code = 1
    elif row_label == 'bicycle':
        code = 2
    elif row_label == 'bus':
        code = 3
    elif row_label == 'car':
        code = 4
    elif row_label == 'motorcycle':
        code = 5
    elif row_label == 'motorized_vehicle':
        code = 6
    elif row_label == 'non-motorized_vehicle':
        code = 7
    elif row_label == 'pedestrian':
        code = 8
    elif row_label == 'pickup_truck':
        code = 9
    elif row_label == 'single_unit_truck':

        code = 10
    elif row_label == 'work_van':
        code = 11
    else:
        code = None
    return code


def split(dataf, group):
    """
    Split CSV data to dictionary
    :param dataf:pd csv data
    :param group:"filename"
    :return:dictionary data
    """
    data = namedtuple('data', ['filename', 'object'])
    groupb = dataf.groupby(group)
    return [data(
        filename, groupb.get_group(x)) for filename, x in zip(
            groupb.groups.keys(), groupb.groups)]


def correction(image_name):
    """
    Convert the image_name from integer type to string
    type which beginning with zeros
    :param image_name: integer image name
    :return: new_name
     """
    new_name = str(image_name)
    if len(new_name) < 8:
        while len(str(new_name)) < 8:
            new_name = "0" + new_name
    return new_name


def create_tf_example(group, path):
    """
    Create an example training model
    :param path: path to images file
    """
    with tf.io.gfile.GFile(
            os.path.join(path + r"/{}.jpg".format(
                correction(group.filename))), 'rb')as fid:
        encoded_jpg = fid.read()
    width, height = Image.open(io.BytesIO(encoded_jpg)).size
    # initialization
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []
    for index, row in group.object.iterrows():
        index = index+1
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class']))

    # Get the tensorflow training model
    tf_example = tf.train.Example(features=tf.train.Features(feature={

        'image/height': dataset_util.int64_feature(height),

        'image/width': dataset_util.int64_feature(width),

        'image/filename': dataset_util.bytes_feature(
            correction(group.filename).encode('utf8')),

        'image/source_id': dataset_util.bytes_feature(
            correction(group.filename).encode('utf8')),

        'image/encoded': dataset_util.bytes_feature(encoded_jpg),

        'image/format': dataset_util.bytes_feature(b'jpg'),

        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),

        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),

        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),

        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),

        'image/object/class/text':
            dataset_util.bytes_list_feature(classes_text),

        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))

    return tf_example


def main(_):
    """
    MAIN function
    """
    writer = tf.io.TFRecordWriter(FLAGS.output_path)
    path = os.path.join(FLAGS.image_dir)
    # Importing Data
    examples = pd.read_csv(FLAGS.csv_input)
    grouped = split(examples, 'filename')
    for group in grouped:
        tf_example = create_tf_example(group, path)
        writer.write(tf_example.SerializeToString())
    writer.close()
    output_path = os.path.join(os.getcwd(), FLAGS.output_path)
    print('Successfully created the TFRecords: {}'.format(output_path))


if __name__ == '__main__':
    tf.compat.v1.app.run()
