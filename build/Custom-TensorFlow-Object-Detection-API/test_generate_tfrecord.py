"""
this is a test file for generate tfrecord
"""
import os
import pytest
import pandas as pd
import generate_tfrecord


@pytest.mark.parametrize('vehicle_type, number',
                         [
                             ('articulated_truck', 1),
                             ('bicycle', 2),
                             ('bus', 3),
                             ('car', 4),
                             ('motorcycle', 5),
                             ('motorized_vehicle', 6),
                             ('non-motorized_vehicle', 7),
                             ('pedestrian', 8),
                             ('pickup_truck', 9),
                             ('single_unit_truck', 10),
                             ('work_van', 11)
                         ]
                         )
def test_class_text_to_int(vehicle_type, number):
    """
    this is a test for text_to_int() function
    :param vehicle_type: all types of vehicle according to the label map
    :param number: the identifier of each vehicle type
    """
    assert generate_tfrecord.class_text_to_int(vehicle_type) == number


@pytest.mark.parametrize('image_name,result',
                         [('8', "00000008"),
                          ('3', "00000003"),
                          ('65', "00000065"),
                          ('120', "00000120")
                          ]
                         )
def test_correction(image_name, result):
    """
    this is a test for correction() function
    """
    assert generate_tfrecord.correction(image_name) == result


@pytest.mark.parametrize('dataf,group,result',
                         [(pd.DataFrame(
                             {'filename': ["00000001"],
                              'class': ["bus"],
                              'xmin': [325],
                              'ymin': [541],
                              'xmax': [654],
                              'ymax': [125]}),
                           'filename',
                           "[data(filename='00000001', object=" +
                           "  class  filename  xmax  xmin  " +
                           "ymax  ymin\n0   bus" +
                           "  00000001   654   325   125   541)]"
                           ), (pd.DataFrame({'filename': ["00000200"],
                                             'class': ["car"],
                                             'xmin': [123],
                                             'ymin': [214],
                                             'xmax': [256],
                                             'ymax': [632]}
                                            ),
                               'filename',
                               "[data(filename='00000200', object=" +
                               "  class  filename  xmax  xmin  ymax  " +
                               "ymin\n0   car" +
                               "  00000200   256   123   632   214)]"
                               )
                          ]
                         )
def test_split(dataf, group, result):
    """
    this is a test for split() function
    """
    assert str(generate_tfrecord.split(dataf, group)) == result
