"""
this is a test for number_of_cars_producer functions
"""
import json
import number_of_cars_producer


def test_params_to_json():
    """
    this is a test for params_to_json function
    """
    data = {
        "id": 1,
        "cars_left": 13,
        "cars_right": 22
    }
    data_dump = json.dumps(data)
    assert number_of_cars_producer.params_to_json(1, 13, 22) == data_dump


def test_connect_to_host():
    """
    this is a test for connect_to_host function
    """
    channel = '(<BlockingChannel impl=<Channel number=1 OPEN conn=<SelectConnection OPEN transport=<pika.adapters.utils.io_services_utils._AsyncPlaintextTransport object at 0x0000022AAEBCE4E0> params=<ConnectionParameters host=localhost port=5672 virtual_host=/ ssl=False>>>>, <BlockingConnection impl=<SelectConnection OPEN transport=<pika.adapters.utils.io_services_utils._AsyncPlaintextTransport object at 0x0000022AAEBCE4E0> params=<ConnectionParameters host=localhost port=5672 virtual_host=/ ssl=False>>>)'
    assert str(number_of_cars_producer.connect_to_host('localhost')) == channel
